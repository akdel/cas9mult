from distutils.core import setup

setup(name='cas9mult',
      version='1.0',
      description='',
      author='Mehmet Akdel',
      author_email='mehmet.akdel@wur.nl',
      url='https://gitlab.com/akdel/',
      scripts=["bin/grna_to_cmap",
               "bin/find_most_common_grnas"],
      packages=['CasMult'],
      install_requires=["scipy", "numpy", "matplotlib", "fire", "numba"])
