# in-silico gRNA discovery and digestion tools for optical mapping

## Requirements:

`python >= 3.6 and pip`

## Installation

```shell script
git clone https://gitlab.com/akdel/cas9mult.git
cd cas9mult
pip install .
```

or

```shell script
pip install git+https://gitlab.com/akdel/cas9mult.git
```

### Usage - insilico gRNA labeling:
```shell script
grna_to_cmap FASTA_FILE_PATH OUT_FILE_NAME GRNA_SEQUENCE <flags>
  optional flags:   --maximum_mismatches | --minimum_score | --channel_id
```
example:
```shell script
grna_to_cmap genome.fasta digested_genome.cmap TCTTCGGTGGAGGTAGGTTA
```

## Usage - gRNA finding
```shell script
find_most_common_grnas FASTA_FILE_PATH NUMBER_OF_GRNAS <flags>
  optional flags:        --maximum_mismatches | --minimum_score | --channel_id
```
example:
```shell script
find_most_common_grnas genome.fasta 50
```
