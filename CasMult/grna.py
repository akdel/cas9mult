import typing

import numpy as np
from typing import List, Iterator, Tuple, Dict, Union
import itertools
from dataclasses import dataclass
import numba as nb
import pickle
import enum
import intervaltree as it
from OptiScan import utils
from glob import glob


# 1. Function to write sites as cmap [soon]
# 2. Functions to find top ranking gRNAs [later]


class CustomGrnas(enum.Enum):
    TCTTCGGTGGAGGTAGGTTA = "TCTTCGGTGGAGGTAGGTTA"
    ACCAGGATAGAATATGGATC = "ACCAGGATAGAATATGGATC"


PAM_SEQ = ("G", "G")
PAM_SEQ_REV = ("C", "C")
OFF_TARGET_WEIGHTS = np.array([0, 0, 0.014, 0, 0, 0.395, 0.317, 0, 0.389, 0.079,
                               0.445, 0.508, 0.613, 0.851, 0.732, 0.828, 0.615, 0.804, 0.685, 0.583])
REVDICT = {"A": "T", "T": "A",
           "C": "G", "G": "C"}


@dataclass
class GeneRange:
    chromosome: str
    start: int
    end: int
    data: str

    @classmethod
    def from_bedfile_line(cls, line, buffer=0) -> "GeneRange":
        line_data = [x.strip() for x in line.split("\t")]
        assert len(line_data) == 4
        chromosome, start, end, data = line_data
        start, end = sorted([int(start), int(end)])
        return cls(str(chromosome), int(start) - buffer, int(end) + buffer, str(data))

    @classmethod
    def from_cmap_line(cls, line_data: list, buffer=500):
        assert len(line_data) == 9
        chromosome, start, = line_data[0], line_data[5]
        buffer /= 2
        return cls(str(chromosome), int(start) - int(buffer), int(start) + int(buffer), "cmap")


GeneRanges = Dict[str, List[GeneRange]]


def generanges_from_bedfile(list_of_generange: List[GeneRange]) -> GeneRanges:
    gene_ranges: GeneRanges = dict()
    for gene_range in list_of_generange:
        if gene_range.chromosome not in gene_ranges:
            gene_ranges[gene_range.chromosome] = [gene_range]
        else:
            gene_ranges[gene_range.chromosome].append(gene_range)
    return gene_ranges


@dataclass
class PamSite:
    sequence_index: int
    orientation: bool
    grna_sequence: Tuple[str]

    @property
    def seq(self):
        return "".join(self.grna_sequence)


def reverse_complement(sequence: Tuple[str]) -> Iterator[str]:
    for s in sequence[::-1]:
        if s in REVDICT:
            yield REVDICT[s]
        else:
            yield s


def find_pam_sites_in_sequence(sequence: Tuple[str]) -> Iterator[
    PamSite]:  # TODO: use some test data to check if the reverse complement is working properly
    for i in range(19, len(sequence) - 22):
        site: Tuple[str] = sequence[i:i + 3]
        if site[1:] == PAM_SEQ:
            yield PamSite(i, True, sequence[i - 20: i])
        elif site[:-1] == PAM_SEQ_REV:
            yield PamSite(i, False, tuple(reverse_complement(sequence[i + 3: i + 23])))
        else:
            continue


def write_pam_sites_to_disk(fasta_file_path: str, out_file: str = "pam_dict.pkl") -> 'ChrPamSites':
    pam_dict: Dict[int, List[PamSite]] = dict()
    _id: int = 0
    for k, g in itertools.groupby(iter(open(fasta_file_path, "r")), key=lambda x: x.startswith(">")):
        if not k:
            seq = "".join(x.strip() for x in g)
            pam_sites = find_pam_sites_in_sequence(tuple(seq))
            pam_dict[_id] = list(pam_sites)
        else:
            _id += 1
    pickle.dump(pam_dict, open(out_file, "wb"))
    return pam_dict


def write_pam_sites_to_disk_with_chr_names(fasta_file_path: str, out_file: str = "pam_dict.pkl") -> 'ChrPamSites':
    pam_dict: Dict[int, List[PamSite]] = dict()
    for k, g in itertools.groupby(iter(open(fasta_file_path, "r")), key=lambda x: x.startswith(">")):
        if k:
            last = list(g)[0][1:].strip()
        else:
            seq = "".join(x.strip() for x in g)
            pam_sites = find_pam_sites_in_sequence(tuple(seq))
            pam_dict[last] = list(pam_sites)
    pickle.dump(pam_dict, open(out_file, "wb"))
    return pam_dict


@dataclass()
class PamFrequencies:
    frequencies: Dict[Tuple[str], int]
    fasta_path: str
    grna_locations: Union[Dict[Tuple[str], List[Tuple[str, int]]], None] = None

    @classmethod
    def from_fasta(cls, fasta_path: str) -> 'PamFrequencies':
        frequencies: Dict[Tuple[str], int] = dict()
        for pam_site in itertools.chain.from_iterable(iter(generate_sequences_from_fasta(fasta_path))):
            if pam_site.grna_sequence not in frequencies:
                frequencies[pam_site.grna_sequence] = 1
            else:
                frequencies[pam_site.grna_sequence] += 1
        return cls(frequencies, fasta_path)

    @classmethod
    def from_fasta_limit_coordinates(cls, fasta_path: str, gene_ranges: GeneRanges,
                                     collect_locations: bool = False) -> 'PamFrequencies':
        frequencies: Dict[Tuple[str], int] = dict()
        locations: Dict[Tuple[str], List[Tuple[str, int]]] = dict()
        for chromosome_pams, chromosome in iter(generate_sequences_from_fasta_with_keys(fasta_path)):
            if chromosome not in gene_ranges:
                continue
            tree = it.IntervalTree(
                [it.Interval(begin=x.start, end=x.end, data=chromosome) for x in gene_ranges[chromosome]])
            if not collect_locations:
                for pam_site in chromosome_pams:
                    if len(tree.at(int(pam_site.sequence_index))):
                        if pam_site.grna_sequence not in frequencies:
                            frequencies[pam_site.grna_sequence] = 1
                        else:
                            frequencies[pam_site.grna_sequence] += 1
            else:
                for pam_site in chromosome_pams:
                    if len(tree.at(int(pam_site.sequence_index))):
                        if pam_site.grna_sequence not in frequencies:
                            frequencies[pam_site.grna_sequence] = 1
                            locations[pam_site.grna_sequence] = [(chromosome, pam_site.sequence_index)]
                        else:
                            frequencies[pam_site.grna_sequence] += 1
                            locations[pam_site.grna_sequence].append((chromosome, pam_site.sequence_index))
        return cls(frequencies, fasta_path, locations)

    def find_top_grna_sequences(self):
        pass

    def write_top_grnas_to_cmap(self, k: int = 10, score_thr: float = 0.25,
                                max_mismatch: int = 3, channel_id: int = 1, numeric_ids: bool = True,
                                path=".") -> None:
        return self.write_top_grnas_to_cmap_from_other(self, k=k, score_thr=score_thr, max_mismatch=max_mismatch,
                                                       channel_id=channel_id, numeric_ids=numeric_ids, root_path=path)

    def write_top_grnas_to_cmap_from_other(self, other: "PamFrequencies", k: int = 10, score_thr: float = 0.25,
                                           max_mismatch: int = 3, channel_id: int = 1, numeric_ids: bool = True,
                                           root_path: str = ".") -> None:
        if numeric_ids:
            pam_sites = write_pam_sites_to_disk(self.fasta_path)
        else:
            pam_sites = write_pam_sites_to_disk_with_chr_names(self.fasta_path)
        for grna, _ in sorted(other.frequencies.items(), key=lambda x: x[1], reverse=True)[: k]:
            grna_str = "".join(grna)
            secondary_path = root_path + "/" + self.fasta_path.split("/")[-1] + f"_{grna_str}.cmap"
            grna_to_cmap_indices(secondary_path, pam_sites, grna,
                                 score_thr=score_thr, max_mismatch=max_mismatch, channel_id=channel_id)

    def write_grnas_to_cmap(self, list_of_grnas, score_thr: float = 0.25,
                            max_mismatch: int = 3, channel_id: int = 1, numeric_ids: bool = True,
                            root_path="."):
        if numeric_ids:
            pam_sites = write_pam_sites_to_disk(self.fasta_path)
        else:
            pam_sites = write_pam_sites_to_disk_with_chr_names(self.fasta_path)
        for grna in list_of_grnas:
            grna_str = "".join(grna)
            secondary_path = root_path + "/" + self.fasta_path.split("/")[-1] + f"_{grna_str}.cmap"
            grna_to_cmap_indices(secondary_path, pam_sites, grna,
                                 score_thr=score_thr, max_mismatch=max_mismatch, channel_id=channel_id)

    def in_silico_label_with_list_of_grna(self, grnas: List[str], score_thr: float = 0.25, max_mismatch: int = 3):
        pam_sites = write_pam_sites_to_disk(self.fasta_path)
        digestion_sites: List[Tuple[int, List[int]]] = list()
        for grna in grnas:
            for chr_id, pams in pam_sites.items():
                digestion_indices = [x.sequence_index for x in pams if
                                     check_with_mismatches(x.grna_sequence, grna, max_mismatch) >= score_thr]
                digestion_sites.append((chr_id, digestion_indices))
        return digestion_sites


def generate_sequences_from_fasta(fasta_file_path: str) -> Iterator[List[PamSite]]:
    for k, g in itertools.groupby(iter(open(fasta_file_path, "r")), key=lambda x: x.startswith(">")):
        if not k:
            seq = "".join(x.strip() for x in g)
            pam_sites = find_pam_sites_in_sequence(tuple(seq))
            yield pam_sites


def generate_sequences_from_fasta_with_keys(fasta_file_path: str) -> Tuple[Iterator[List[PamSite]], str]:
    last = ""
    for k, g in itertools.groupby(iter(open(fasta_file_path, "r")), key=lambda x: x.startswith(">")):
        if k:
            last = list(g)[0][1:].strip()
        else:
            seq = "".join(x.strip() for x in g)
            pam_sites = find_pam_sites_in_sequence(tuple(seq))
            yield pam_sites, last


def cmap_to_generanges(cmap_file_location: str):
    parser = utils.CmapParser(cmap_file_location, ini=False)
    parser.read_and_load_cmap_file()
    return generanges_from_bedfile([GeneRange.from_cmap_line(x, buffer=0)
                                    for x in parser.cmap_lines if x[4] != "0" and len(x) == 9])


def bedfile_to_generanges(bedfile_location: str):
    return generanges_from_bedfile([GeneRange.from_bedfile_line(x.strip(), buffer=0)
                                    for x in iter(open(bedfile_location, "r"))])


def check_genes(original_generanges: GeneRanges,
                grna_label_ranges: GeneRanges,
                banned_genes: Union[None, typing.Set[Tuple[int, int, str]]] = None) -> Tuple[
    Dict[Tuple[int, int, str], int], int, int]:
    if banned_genes is None:
        banned_genes = set()
    all_genes: Dict[Tuple[int, int, str], int] = dict()
    hits: int = 0
    no_hits: int = 0
    for chromosome, genes in original_generanges.items():
        for gene in genes:
            if (gene.start, gene.end, gene.chromosome) not in banned_genes:
                all_genes[(gene.start, gene.end, gene.chromosome)] = 0
    for chromosome in original_generanges.keys():
        if chromosome in grna_label_ranges:
            tree = it.IntervalTree([it.Interval(begin=x.start, end=x.end, data=(x.start, x.end, chromosome))
                                    for x in original_generanges[chromosome]])
            for gene in grna_label_ranges[chromosome]:
                if len(tree.at(int(gene.start))):
                    hits += 1
                else:
                    no_hits += 1
                for interval in tree.at(int(gene.start)):
                    if interval.data in all_genes:
                        all_genes[interval.data] += 1
    return all_genes, hits, no_hits


def find_best_cmap(cmap_files: List[str],
                   gene_bedfile: str,
                   top_k: int = 3,
                   banned_genes: Union[set, None] = None) -> List[
    Tuple[str, float, float, typing.Set[Tuple[int, int, str]]]]:
    if banned_genes is None:
        banned_genes = set()
    scores: List[Tuple[str, float, float, typing.Set[Tuple[int, int, str]]]] = list()
    bedfile_generanges = bedfile_to_generanges(gene_bedfile)
    for i, cmap in enumerate(cmap_to_generanges(f) for f in cmap_files):
        covered, hits, no_hits = check_genes(bedfile_generanges, cmap, banned_genes=banned_genes)
        perc = round(len([x for x in covered.values() if x != 0]) / len(list(covered.values())) * 100)
        print(perc, hits, no_hits)
        scores.append(
            (cmap_files[i],
             perc,
             hits / (no_hits + hits + 1) * 100,
             banned_genes | {k for k, v in covered.items() if v > 0}
             ))
    return sorted(scores, key=lambda x: x[2] * x[1], reverse=True)[: top_k]


def filter_gene_ranges(generanges: GeneRanges, banned_genes: typing.Set[Tuple[int, int, str]]):
    banned_genes_dict = {}
    for start, end, chr_id in banned_genes:
        if chr_id not in banned_genes_dict:
            banned_genes_dict[chr_id] = {(start, end)}
        else:
            banned_genes_dict[chr_id].add((start, end))
    for chr_id, genes in banned_genes_dict.items():
        generanges[chr_id] = [x for x in generanges[chr_id] if (x.start, x.end) not in genes]
    return generanges


def find_best_covering_grnas(bedfile: str, genome_fastafile: str, cmap_folder: str,
                             banned_genes: set, top_score: int = 3, top_k: int = 20):
    bed_generanges = bedfile_to_generanges(bedfile)
    PamFrequencies \
        .from_fasta_limit_coordinates(genome_fastafile, bed_generanges, collect_locations=True) \
        .write_top_grnas_to_cmap(
        k=top_k,
        score_thr=0.5,
        max_mismatch=3,
        channel_id=1,
        numeric_ids=False,
        path=cmap_folder
    )
    return find_best_cmap(glob(str(cmap_folder) + "/*cmap"), bedfile, top_k=top_score, banned_genes=banned_genes)


def optimize_gRNA_for_genes(genome_fastafile: str,
                            bedfile: str,
                            cmap_folder: str,
                            top_k: int = 20,
                            top_score: int = 3,
                            iterations: int = 3):
    from pathlib import Path
    cmap_folder = Path(cmap_folder)
    if cmap_folder.exists():
        _ = [x.unlink() for x in cmap_folder.glob("*")]
        cmap_folder.rmdir()
    cmap_folder.mkdir()
    current_best = find_best_covering_grnas(bedfile, genome_fastafile, str(cmap_folder), banned_genes=set(),
                                            top_score=top_score, top_k=top_k)
    print(current_best)
    best_grna = [current_best]
    for i in range(iterations - 1):
        new_best = []
        for f_name, perc, hits, banned_genes in current_best:
            _ = [x.unlink() for x in cmap_folder.glob("*")]
            best = find_best_covering_grnas(bedfile, genome_fastafile, str(cmap_folder), banned_genes=banned_genes,
                                            top_score=top_score, top_k=top_k)
            for f_name2, perc2, hits2, banned_genes2 in best:
                ext_fname = f_name.split("/")[-1]
                new_best.append((f"{f_name2}-{ext_fname}", perc2, hits2, banned_genes2))
        current_best = new_best
        best_grna.append(new_best)
        print(new_best)
    return best_grna


@nb.njit
def calc_grna_score(t1, n, d):
    t2 = 1 / ((19 - d) / 19 * 4 + 1)
    t3 = 1 / (n ** 2)
    return t1 * t2 * t3


def check_with_mismatches(sequence, grna, max_mismatch):
    n = 0
    mismatches = np.zeros(len(sequence), dtype=np.int8)
    T1 = 1
    for i in range(len(sequence)):
        if n > max_mismatch:
            return 0
        elif sequence[i] == grna[i]:
            pass
        else:
            mismatches[n] = i
            T1 *= 1 - OFF_TARGET_WEIGHTS[i]
            n += 1
    if n == 0:
        return 1
    elif n == 1:
        d = 19
    else:
        d = (np.max(mismatches[:n]) - np.min(mismatches[:n])) / (n - 1)
    return calc_grna_score(T1, n, d)


ChrPamSites = Dict[int, List[PamSite]]


def grna_to_cmap_indices(out_file: str,
                         pam_sites: Union[str, ChrPamSites],
                         grna_motif: str = "AAAAAAAAAAAAAAAAAA",
                         channel_id: int = 1,
                         score_thr: float = 0.25,
                         max_mismatch: int = 3):
    assert len(grna_motif) == 20

    if type(pam_sites) == str:
        pam_sites: ChrPamSites = pickle.load(open(pam_sites, "rb"))
    else:
        pass
    of = open(out_file, "w")

    for chr_id, pams in pam_sites.items():
        digestion_indices = [x.sequence_index for x in pams if
                             check_with_mismatches(x.grna_sequence, grna_motif, max_mismatch) >= score_thr]
        try:
            last_index = digestion_indices[-1] + 1
        except IndexError:
            continue
        for i in range(len(digestion_indices)):
            of.write(
                f"{chr_id}\t{last_index}\t{len(digestion_indices)}\t{i + 1}\t{channel_id}\t{digestion_indices[i]}\t1.0\t1\t1\n"
            )
        of.write(
            f"{chr_id}\t{last_index}\t{len(digestion_indices)}\t{len(digestion_indices) + 1}\t{0}\t{last_index}\t1.0\t1\t1\n")
    of.close()


def replace_motif_with_cmap_info(fasta_file, cmap_file, motif_to_replace, output_filename: str = "out.fasta"):
    # 1. get_grna_locations from cmap with fasta headers
    cmap_locations: GeneRanges = cmap_to_generanges(cmap_file)

    # 2. open fasta file with groupby
    output_file = open(output_filename, "w")
    fasta_header = ""
    for k, g in itertools.groupby(iter(open(fasta_file, "r")), lambda x: x.startswith(">")):
        if k:
            fasta_header = list(g)[0][1:].strip()
            output_file.write(f">{fasta_header}\n")

        # 3. whenever the header matches, replace the sequence at coordinates with the new motif
        elif fasta_header in cmap_locations:
            current_ranges = cmap_locations[fasta_header]
            sequence = "".join(x.strip() for x in g)
            for single_range in current_ranges:
                if single_range.start + len(motif_to_replace) < len(sequence):
                    sequence = sequence[:single_range.start] + motif_to_replace + sequence[single_range.start + len(motif_to_replace) - 1:]
            # 4. write the result to a new fasta file
            output_file.write(sequence)
        else:
            sequence = "".join(x.strip() for x in g)
            output_file.write(sequence)
    output_file.close()

    return output_filename
