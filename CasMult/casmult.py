#!/usr/bin python

from dataclasses import dataclass
import typing as ty
import itertools
import enum
import numpy as np
import numba as nb


@nb.njit
def sliding_average(input_array: np.ndarray, size: int = 1000, remove_zeros: bool = True) -> np.ndarray:
    res: np.ndarray = np.zeros(input_array.shape[0] - size + 1)

    for i in range(0, input_array.shape[0] - size + 1):
        slice_ = input_array[i:i + size]
        if remove_zeros:
            slice_ = slice_[slice_ != 0]
            slice_sum = slice_.sum()
            if slice_sum == 0:
                res[i] = 0
            else:
                res[i] = np.mean(slice_)
        else:
            res[i] = np.mean(slice_)
    return res


class Channel(enum.Enum):
    Single = 1
    Dual = 2


Snr = float
Label = float


@dataclass
class BnxMolecule:
    type: Channel
    channel_1_labels: ty.List[ty.Tuple[Label, Snr]]
    channel_2_labels: ty.List[ty.Tuple[Label, Snr]]
    bnx_id: int
    length: float

    @classmethod
    def from_lines(cls, lines: ty.List[str]) -> 'BnxMolecule':
        if len(lines):
            info: ty.List[str] = [x.strip() for x in lines[0].split("\t")]
        else:
            raise Warning("no lines found")

        if len(lines) == 4:
            channel: Channel = Channel.Single
            labels: ty.List[float] = [float(x) for x in lines[1].split()[1:]][:-1]
            snrs: ty.List[float] = [float(x) for x in lines[2].split()[1:]]
            assert len(labels) == len(snrs)
            return cls(channel,
                       [(labels[i], snrs[i]) for i in range(len(labels))],
                       list(),
                       int(info[1]),
                       float(info[2]))

        elif len(lines) == 7:
            channel: Channel = Channel.Dual
            c1_labels: ty.List[float] = [float(x) for x in lines[1].split()[1:]][:-1]
            c2_labels: ty.List[float] = [float(x) for x in lines[2].split()[1:]][:-1]
            c1_snrs: ty.List[float] = [float(x) for x in lines[3].split()[1:]]
            c2_snrs: ty.List[float] = [float(x) for x in lines[4].split()[1:]]
            assert (len(c1_labels) == len(c1_snrs)) and (len(c2_labels) == len(c2_snrs))
            return cls(channel,
                       [(c1_labels[i], c1_snrs[i]) for i in range(len(c1_labels))],
                       [(c2_labels[i], c2_snrs[i]) for i in range(len(c2_labels))],
                       int(info[1]),
                       float(info[2]))
        else:
            raise Warning("wrong number of lines")


def generate_bnx_molecules(bnx_filename: str, channel: Channel = Channel.Single) -> ty.Iterator[BnxMolecule]:
    lines: ty.List[str] = list()
    for k, g in itertools.groupby(iter(open(bnx_filename, "r")), lambda x: x.split("\t")[0]):
        if k.startswith("#"):
            continue
        else:
            if (len(lines) == 4 and channel == Channel.Single) or (len(lines) == 7 and channel == Channel.Dual):
                yield BnxMolecule.from_lines(lines)
                lines: ty.List[str] = [list(g)[0]]
            else:
                lines.append(list(g)[0])


def get_chromosomes_from_cmap(cmap_file_location: str) -> 'Cmap':
    chromosomes: 'Cmap' = dict()
    for k, g in itertools.groupby((x.split("\t") for x in open(cmap_file_location, "r") if not x.startswith("#")),
                                  key=lambda x: int(x[0])):
        chromosomes[k] = CmapChromosome.from_chr_cmap_details(list(g), k)
    return chromosomes


@dataclass
class CmapChromosome:
    chromosome_id: int
    number_of_labels: int
    labels: ty.List[float]

    @classmethod
    def from_chr_cmap_details(cls, details: ty.List[ty.List[str]], chr_id: int) -> 'CmapChromosome':
        number_of_labels: int = int(details[0][2])
        labels: ty.List[float] = list()
        for detail in details:
            if int(detail[4]) != 0:
                labels.append(float(detail[5]))
            else:
                continue
        return cls(chr_id, number_of_labels, labels)


Cmap = ty.Dict[int, CmapChromosome]


@dataclass
class XmapMatch:
    molecule_id: int
    reference_id: int
    molecule_start: float
    reference_start: float
    molecule_end: float
    reference_end: float
    orientation: bool
    confidence: float

    @classmethod
    def from_xmap_line(cls, line: str) -> 'XmapMatch':
        line = [x.strip() for x in line.split("\t")]
        assert len(line) == 14
        molecule_id: int = int(line[1])
        reference_id: int = int(line[2])
        molecule_start: float = float(line[3])
        reference_start: float = float(line[5])
        molecule_end: float = float(line[4])
        reference_end: float = float(line[6])
        orientation: bool = True if line[7] == "+" else False
        confidence: float = float(line[8])
        return cls(molecule_id, reference_id, molecule_start,
                   reference_start, molecule_end, reference_end,
                   orientation, confidence)


def generate_xmap_matches(filename: str):
    return (XmapMatch.from_xmap_line(x) for x in open(filename, "r") if not x.startswith("#"))


MoleculeID = int


@dataclass
class Reference:
    dle_labels: ty.Dict[int, ty.List[float]]
    cas9_labels: ty.Dict[int, ty.List[float]]

    @classmethod
    def from_cmaps(cls, dle_cmap, cas9_cmap):
        pass


@dataclass
class XmapAndMols:
    matched_channel: Channel
    matches: ty.Dict[MoleculeID, XmapMatch]
    molecules: ty.Dict[MoleculeID, BnxMolecule]
    reference: Cmap

    @dataclass
    class LabelStats:
        reference_label_density: float
        molecule_label_density: float
        matched_label_distances: ty.List[float]
        mismatches: float
        match_distance_threshold: float

    class TestType(enum.Enum):
        Precision = 1
        Recall = 2
        F1Score = 3
        Accuracy = 4
        Correct = 5
        Incorrect = 6

    @classmethod
    def from_xmap_and_bnx(cls,
                          bnx_file_path: str,
                          xmap_file_path: str,
                          channel_type: Channel,
                          cmap_file_path: str) -> 'XmapAndMols':
        bnx_molecules: ty.Dict[MoleculeID, BnxMolecule] = {x.bnx_id: x for x in
                                                           generate_bnx_molecules(bnx_file_path, channel_type)}
        xmap_matches: ty.Dict[MoleculeID, XmapMatch] = {x.molecule_id: x for x in generate_xmap_matches(xmap_file_path)}
        chromosomes: Cmap = get_chromosomes_from_cmap(cmap_file_path)
        return cls(channel_type, xmap_matches, bnx_molecules, chromosomes)

    def get_label_density(self,
                          channel: Channel,
                          reference: ty.Union[None, Cmap] = None,
                          in_silico: bool = False,
                          zoom: int = 1) -> float:
        total_length: float = 0.
        total_number_of_labels: float = 0.
        for mol_id in self.matches.keys():
            if not in_silico:
                _, matched_molecule_fragment = self.get_aligned_coordinates_from_xmap(mol_id,
                                                                                      channel,
                                                                                      reference=reference,
                                                                                      zoom=zoom)
            else:
                matched_molecule_fragment, _ = self.get_aligned_coordinates_from_xmap(mol_id,
                                                                                      channel,
                                                                                      reference=reference,
                                                                                      zoom=zoom)
            total_length += self.molecules[mol_id].length
            total_number_of_labels += len(matched_molecule_fragment)
        return (total_number_of_labels / total_length) * 100_000

    def get_label_stats(self,
                        channel: Channel,
                        reference: ty.Union[None, Cmap] = None,
                        match_distance: float = 15_000,
                        random_reference: bool = False,
                        zoom: int = 1) -> 'LabelStats':
        reference_label_density = self.get_label_density(channel, reference=reference, in_silico=True, zoom=zoom)
        molecule_label_density = self.get_label_density(channel, reference=reference, in_silico=False, zoom=zoom)
        matching_label_distances: ty.List[float] = list()
        number_of_mismatches: int = 0
        for molecule_id in self.matches.keys():
            reference_fragment, matched_molecule_fragment = self.get_aligned_coordinates_from_xmap(molecule_id,
                                                                                                   channel,
                                                                                                   reference=reference,
                                                                                                   zoom=zoom)
            reference_fragment, matched_molecule_fragment = np.array(reference_fragment), np.array(
                matched_molecule_fragment)
            if random_reference:
                range_of_val = np.arange(
                    np.abs(int(self.matches[molecule_id].molecule_end - self.matches[molecule_id].molecule_start)))
                reference_fragment = np.random.choice(range_of_val,
                                                      len(matched_molecule_fragment)).astype(float)
            for i in range(reference_fragment.shape[0]):
                if not len(matched_molecule_fragment):
                    continue
                else:
                    shortest: float = np.abs(matched_molecule_fragment - reference_fragment[i]).min()
                    if shortest <= match_distance:
                        matching_label_distances.append(shortest)
                    else:
                        number_of_mismatches += 1
        return self.LabelStats(reference_label_density, molecule_label_density,
                               matching_label_distances,
                               number_of_mismatches / (len(matching_label_distances) + number_of_mismatches),
                               match_distance)

    def get_difference(self, xmap_and_mols: 'XmapAndMols') -> 'XmapAndMols':
        difference: ty.Dict[MoleculeID, XmapMatch] = {x: self.matches[x] for x in
                                                      set(self.matches.keys()) - set(xmap_and_mols.matches.keys())}
        return XmapAndMols(self.matched_channel,
                           difference,
                           self.molecules,
                           self.reference)

    def get_intersection(self, xmap_and_mols: 'XmapAndMols', higher_conf: bool = False):
        if not higher_conf:
            union: ty.Dict[MoleculeID, XmapMatch] = {x: self.matches[x] for x in set(self.matches.keys()).intersection(
                set(xmap_and_mols.matches.keys()))}
        else:
            union: ty.Dict[MoleculeID, XmapMatch] = {x: self.matches[x] for x in set(self.matches.keys()).intersection(
                set(xmap_and_mols.matches.keys())) if self.matches[x].confidence > xmap_and_mols.matches[x].confidence}
        return XmapAndMols(self.matched_channel,
                           union,
                           self.molecules,
                           self.reference)

    def score_molecule_with_nearest_cas9_site(self,
                                              molecule_id: int,
                                              channel: Channel,
                                              match_distance: float = 15_000,
                                              match_score: float = 1.,
                                              mismatch_score: float = 1.,
                                              reference: ty.Union[None, Cmap] = None,
                                              zoom: int = 500):

        reference_fragment, matched_molecule_fragment = self.get_aligned_coordinates_from_xmap(molecule_id,
                                                                                               channel,
                                                                                               reference=reference,
                                                                                               zoom=zoom)
        reference_fragment, matched_molecule_fragment = np.array(reference_fragment), np.array(
            matched_molecule_fragment)
        score: float = 0
        for i in range(reference_fragment.shape[0]):
            if not len(matched_molecule_fragment):
                continue
            else:
                shortest: float = np.abs(matched_molecule_fragment - reference_fragment[i]).min()
                if shortest <= match_distance:
                    score += match_score
                else:
                    score -= mismatch_score
        return score

    def score_molecule_with_nearest_cas9_site2(self,
                                               molecule_id: int,
                                               channel: Channel,
                                               match_distance: float = 15_000,
                                               match_score: float = 1.,
                                               mismatch_score: float = 1.,
                                               reference: ty.Union[None, Cmap] = None,
                                               extra_penalty: float = 1.,
                                               zoom: int = 500,
                                               only_mismatch: bool = False):
        reference_fragment, matched_molecule_fragment = self.get_aligned_coordinates_from_xmap(molecule_id,
                                                                                               channel,
                                                                                               reference=reference,
                                                                                               zoom=zoom)
        reference_fragment, matched_molecule_fragment = np.array(reference_fragment), np.array(
            matched_molecule_fragment)
        score: float = 0
        extra: float = 0
        matched = 0
        mismatches = 0
        for i in range(reference_fragment.shape[0]):
            if not len(matched_molecule_fragment):
                continue
            else:
                shortest: float = np.abs(matched_molecule_fragment - reference_fragment[i]).min()
                if shortest <= match_distance:
                    score += (1 - (shortest / match_distance)) * match_score
                    matched += 1
                else:
                    mismatches += mismatch_score
        if len(matched_molecule_fragment) and (matched <= len(matched_molecule_fragment)):
            extra += ((len(matched_molecule_fragment) - matched) / (len(matched_molecule_fragment)))
        if only_mismatch:
            return mismatches
        return (score - mismatches) - (extra * extra_penalty)

    def compare_shared_molecules(self,
                                 other: 'XmapAndMols') -> ty.Iterator[ty.Tuple[int, float]]:
        for molecule_id in self.matches:
            if molecule_id in other.matches:
                this_mol = self.matches[molecule_id]
                other_mol = other.matches[molecule_id]
                difference = this_mol.confidence - other_mol.confidence
                yield molecule_id, difference

    def compare_molecule_to_random(self, molecule_id: int,
                                   channel: Channel,
                                   match_distance: float = 15_000,
                                   reference: ty.Union[None, Cmap] = None,
                                   zoom: int = 500):
        reference_fragment, matched_molecule_fragment = self.get_aligned_coordinates_from_xmap(molecule_id, channel,
                                                                                               reference=reference,
                                                                                               zoom=zoom)
        range_of_val = np.arange(
            np.abs(int(self.matches[molecule_id].molecule_end - self.matches[molecule_id].molecule_start)))
        random_fragment = np.random.choice(range_of_val,
                                           len(matched_molecule_fragment)).astype(float)
        reference_fragment, matched_molecule_fragment = np.array(reference_fragment), np.array(
            matched_molecule_fragment)
        matched_real, matched_random = 0, 0
        for i in range(reference_fragment.shape[0]):
            if not len(matched_molecule_fragment):
                continue
            else:
                shortest_real: float = np.abs(matched_molecule_fragment - reference_fragment[i]).min()
                shortest_random: float = np.abs(random_fragment - reference_fragment[i]).min()
                if (shortest_real <= match_distance) and (shortest_random <= match_distance):
                    if shortest_real < shortest_random:
                        matched_real += 1
                    elif shortest_random < shortest_real:
                        matched_random += 1
                    else:
                        pass
                elif shortest_real <= match_distance:
                    matched_real += 1
                elif shortest_random <= match_distance:
                    matched_random += 1
                else:
                    continue
        return matched_real, matched_random

    def real_to_random_scores(self, channel: Channel, match_distance: float = 15_000,
                              reference: ty.Union[None, Cmap] = None):
        total_real, total_random = 0, 0
        for real, random in (self.compare_molecule_to_random(x, channel, match_distance, reference) for x in
                             self.matches.keys()):
            total_real += real
            total_random += random
        return total_real, total_random

    def allocate_molecules(self,
                           reference1: ty.Union[None, Cmap],
                           reference2: ty.Union[None, Cmap],
                           channel: Channel,
                           match_distance: float = 15_000,
                           match_score: float = 1.,
                           mismatch_score: float = 1.) -> ty.Dict[MoleculeID, ty.Tuple[str, float]]:
        allocations: ty.Dict[MoleculeID, ty.Tuple[str, float]] = dict()
        for molecule_id in self.matches.keys():
            reference1_score = self.score_molecule_with_nearest_cas9_site(molecule_id, channel, reference=reference1,
                                                                          match_distance=match_distance,
                                                                          match_score=match_score,
                                                                          mismatch_score=mismatch_score)
            reference2_score = self.score_molecule_with_nearest_cas9_site(molecule_id, channel, reference=reference2,
                                                                          match_distance=match_distance,
                                                                          match_score=match_score,
                                                                          mismatch_score=mismatch_score)
            if reference1_score > reference2_score:
                allocations[molecule_id] = ("reference 1", reference1_score - reference2_score)
            elif reference1_score == reference2_score:
                allocations[molecule_id] = ("unknown", 0.)
            else:
                allocations[molecule_id] = ("reference 2", reference2_score - reference1_score)
        return allocations

    def get_scores_for_single_ref(self, channel: Channel, reference: Cmap, molecule_ids: ty.Iterator[int],
                                  match_distance: float = 15_000,
                                  match_score: float = 1.,
                                  mismatch_score: float = 1.,
                                  extra_penalty: float = 1.,
                                  only_mismatch: bool = False,
                                  zoom: int = 500) -> ty.Iterator[float]:
        for molecule_id in molecule_ids:
            yield self.score_molecule_with_nearest_cas9_site2(molecule_id, channel, reference=reference,
                                                              match_distance=match_distance,
                                                              match_score=match_score,
                                                              mismatch_score=mismatch_score,
                                                              extra_penalty=extra_penalty,
                                                              only_mismatch=only_mismatch,
                                                              zoom=zoom)

    def allocate_molecules2(self,
                            reference1: ty.Union[None, Cmap],
                            reference2: ty.Union[None, Cmap],
                            channel: Channel,
                            match_distance: float = 15_000,
                            match_score: float = 1.,
                            mismatch_score: float = 1.,
                            extra_penalty: float = 1.,
                            norm_ref1: float = 1.,
                            norm_ref2: float = 1.,
                            zoom: int = 500,
                            score_it: bool = False) -> ty.Dict[MoleculeID, ty.Tuple[str, float, float, float]]:
        allocations: ty.Dict[MoleculeID, ty.Tuple[str, float, float, float]] = dict()
        for molecule_id in self.matches.keys():
            reference1_score = self.score_molecule_with_nearest_cas9_site2(molecule_id, channel, reference=reference1,
                                                                           match_distance=match_distance,
                                                                           match_score=match_score,
                                                                           mismatch_score=mismatch_score,
                                                                           extra_penalty=extra_penalty,
                                                                           zoom=zoom) / norm_ref1
            reference2_score = self.score_molecule_with_nearest_cas9_site2(molecule_id, channel, reference=reference2,
                                                                           match_distance=match_distance,
                                                                           match_score=match_score,
                                                                           mismatch_score=mismatch_score,
                                                                           extra_penalty=extra_penalty,
                                                                           zoom=zoom) / norm_ref2
            if not score_it:
                if reference1_score > reference2_score:
                    allocations[molecule_id] = ("reference 1", reference1_score - reference2_score,
                                                reference1_score, reference2_score)
                elif reference1_score == reference2_score:
                    allocations[molecule_id] = ("unknown", 0., 0., 0.)
                else:
                    allocations[molecule_id] = ("reference 2", reference1_score - reference2_score,
                                                reference1_score, reference2_score)
            else:
                match = self.matches[molecule_id]
                if reference1_score > reference2_score:
                    allocations[molecule_id] = ("correct", reference1_score - reference2_score, match)
                elif reference1_score == reference2_score:
                    allocations[molecule_id] = ("unknown", 0., match)
                else:
                    allocations[molecule_id] = ("incorrect", reference1_score - reference2_score, match)
        return allocations

    def get_coverage(self, zoom: int = 500) -> ty.Dict[int, np.ndarray]:
        coverages: ty.Dict[int, np.ndarray] = {k: np.zeros(int(np.max(v.labels) / zoom)) + 0.00001 for k, v in
                                               self.reference.items()}
        for molecule_id, match in self.matches.items():
            coverages[match.reference_id][int(match.reference_start / zoom): int(match.reference_end / zoom)] += 1.
        return coverages

    def plot_test(self,
                  correct_reference: ty.Union[None, Cmap],
                  incorrect_reference: ty.Union[None, Cmap],
                  channel: Channel,
                  test_type: TestType = TestType.Recall,
                  match_distance: float = 15_000,
                  match_score: float = 1.,
                  mismatch_score: float = 1.,
                  extra_penalty: float = 1.,
                  zoom: int = 500,
                  allocation_zoom: int = 150,
                  window_size: int = 1000):
        coverages: ty.Dict[int, np.ndarray] = self.get_coverage(zoom=zoom)
        allocated_coverages: ty.Dict[int, np.ndarray] = {k: np.zeros(v.shape[0]) for k, v in coverages.items()}
        allocated_coverages_incorrect: ty.Dict[int, np.ndarray] = {k: np.zeros(v.shape[0]) for k, v in
                                                                   coverages.items()}
        for molecule_id, result in self.allocate_molecules2(correct_reference, incorrect_reference,
                                                            channel,
                                                            match_distance=match_distance,
                                                            match_score=match_score,
                                                            mismatch_score=mismatch_score,
                                                            extra_penalty=extra_penalty,
                                                            zoom=allocation_zoom,
                                                            score_it=True).items():
            match: XmapMatch = result[-1]
            if result[0] == "correct":
                allocated_coverages[match.reference_id][
                int(match.reference_start / zoom): int(match.reference_end / zoom)] += 1
            elif result[0] == "incorrect":
                allocated_coverages_incorrect[match.reference_id][
                int(match.reference_start / zoom): int(match.reference_end / zoom)] += 1
        if test_type == self.TestType.Recall:
            return {k: sliding_average(np.nan_to_num((v + allocated_coverages_incorrect[k]) / coverages[k]), size=window_size, remove_zeros=False) for k, v
                    in allocated_coverages.items()}
        elif test_type == self.TestType.Precision:
            return {k: sliding_average(np.nan_to_num((v / (v + allocated_coverages_incorrect[k]) + 0.001)), size=window_size) for k, v in
                    allocated_coverages.items()}
        elif test_type == self.TestType.Correct:
            return {k: sliding_average(np.nan_to_num(allocated_coverages[k]), size=window_size) for k, v in
                    allocated_coverages.items()}
        elif test_type == self.TestType.Incorrect:
            return {k: sliding_average(np.nan_to_num(allocated_coverages_incorrect[k]), size=window_size) for k, v in
                    allocated_coverages.items()}
        else:
            return None

    def get_aligned_coordinates_from_xmap(self,
                                          molecule_id: int,
                                          channel: Channel,
                                          plot: bool = False,
                                          reference: ty.Union[None, Cmap] = None,
                                          zoom: int = 500) -> ty.Tuple[
        ty.List[float], ty.List[float]]:
        match: XmapMatch = self.matches[molecule_id]
        molecule = self.molecules[molecule_id]
        if reference is None:
            reference = self.reference
        reference_fragment: ty.List[float] = [x for x in reference[match.reference_id].labels if
                                              match.reference_start <= x <= match.reference_end]
        reference_fragment: np.ndarray = np.unique(
            (np.array([x - match.reference_start for x in reference_fragment]) // zoom).astype(int))
        reference_fragment: ty.List[float] = list(reference_fragment.astype(float) * zoom)
        if not match.orientation:
            molecule_end = molecule.length - match.molecule_end
            molecule_start = molecule.length - match.molecule_start
            if channel == Channel.Single:
                labels = list(sorted([molecule.length - x[0] for x in molecule.channel_1_labels]))
            else:
                labels = list(sorted([molecule.length - x[0] for x in molecule.channel_2_labels]))
        else:
            molecule_start = match.molecule_start
            molecule_end = match.molecule_end
            if channel == Channel.Single:
                labels = [x[0] for x in molecule.channel_1_labels]
            else:
                labels = [x[0] for x in molecule.channel_2_labels]
        matched_molecule_fragment: ty.List[float] = [x - molecule_start for x in
                                                     labels if
                                                     molecule_start <= x <= molecule_end]
        matched_molecule_fragment: np.ndarray = np.unique((np.array(matched_molecule_fragment) // zoom).astype(int))
        matched_molecule_fragment: ty.List[float] = list(matched_molecule_fragment.astype(float) * zoom)
        if plot:
            import matplotlib.pyplot as plt
            plt.figure(figsize=(15, 2))
            plt.scatter(reference_fragment, [0] * len(reference_fragment), marker="|", s=5000, c="r")
            plt.scatter(matched_molecule_fragment, [1] * len(matched_molecule_fragment), marker="|", s=5000, c="b")
            plt.show()
        return reference_fragment, matched_molecule_fragment


def compare_xmap_and_mols(this: XmapAndMols, other: XmapAndMols,
                          this_reference: Cmap, other_reference: Cmap,
                          this_channel: Channel, other_channel: Channel,
                          match_distance: float = 20_000,
                          match_score: float = 1.,
                          mismatch_score: float = 0.,
                          extra_penalty: float = 0.1):
    dle_results = list(this.compare_shared_molecules(other))
    this_scores = list(this.get_scores_for_single_ref(reference=this_reference,
                                                      channel=this_channel,
                                                      molecule_ids=(x[0] for x in dle_results),
                                                      match_distance=match_distance,
                                                      match_score=match_score,
                                                      mismatch_score=mismatch_score,
                                                      extra_penalty=extra_penalty))
    other_scores = list(other.get_scores_for_single_ref(reference=other_reference,
                                                        channel=other_channel,
                                                        molecule_ids=(x[0] for x in dle_results),
                                                        match_distance=match_distance,
                                                        match_score=match_score,
                                                        mismatch_score=mismatch_score,
                                                        extra_penalty=extra_penalty))
    return [(dle_results[i][0], dle_results[i][1], this_scores[i], other_scores[i]) for i in range(len(dle_results))]


def get_discriminatory_map(cmap1: Cmap, cmap2: Cmap, nearest: float = 2000.) -> ty.Tuple[Cmap, Cmap]:
    discrim_map1: Cmap = dict()
    discrim_map2: Cmap = dict()
    for k, v in cmap1.items():
        new_labels_map1: ty.List[float] = list()
        new_labels_map2: ty.List[float] = list()
        current_chr_id: int = k
        chr_a = np.array(v.labels)
        chr_b = np.array(cmap2[k].labels)
        for a in chr_a:
            closest: float = np.abs(a - chr_b).min()
            if closest > nearest:
                new_labels_map1.append(a)
            else:
                continue
        discrim_map1[current_chr_id] = CmapChromosome(number_of_labels=len(new_labels_map1),
                                                      labels=new_labels_map1,
                                                      chromosome_id=current_chr_id)
        for b in chr_b:
            closest: float = np.abs(b - chr_a).min()
            if closest > nearest:
                new_labels_map2.append(b)
            else:
                continue
        discrim_map2[current_chr_id] = CmapChromosome(number_of_labels=len(new_labels_map2),
                                                      labels=new_labels_map2,
                                                      chromosome_id=current_chr_id)
    return discrim_map1, discrim_map2
